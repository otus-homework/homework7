﻿using ChatCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient
{
    public class Client
    {
        // Порт для отправки сообщений
        private readonly int _remotePort;
        // IP адрес сервера
        private readonly IPAddress _ipAddress;
        // Сокет
        private  Socket _listeningSocket;
        // Имя пользователя
        private string _userName;

        public Client(int remotePort, IPAddress ipAddress, string userName)
        {
            _remotePort = remotePort;
            _ipAddress = ipAddress;
            _userName = userName;
        }

        /// <summary>
        /// Старт клиента
        /// </summary>
        public void Start()
        {
            Console.WriteLine("Клиент запущен!");
            Console.WriteLine("Для отправки сообщений введите сообщение и нажмите Enter");
            Console.WriteLine();
            try
            {
                // Создание сокета для прослушки
                _listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp); 
                Task listeningTask = new Task(Listen); 
                listeningTask.Start();

                var maxMessageSize = Int32.Parse(ConfigurationManager.AppSettings["MaxMessageSize"]);

                while (true) 
                {
                    string body = Console.ReadLine();

                    byte[] data = new Message(_userName, body).Serialize();
                    if (data.Length > maxMessageSize)
                    {
                        Console.WriteLine("Сообщение больше максимально допустимого.");
                        continue;
                    }

                    EndPoint remotePoint = new IPEndPoint(_ipAddress, _remotePort);
                    _listeningSocket.SendTo(data, remotePoint);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Stop();
            }
        }

        /// <summary>
        /// Остановка клиента
        /// </summary>
        public void Stop()
        {
            if (_listeningSocket != null)
            {
                _listeningSocket.Shutdown(SocketShutdown.Both);
                _listeningSocket.Close();
                _listeningSocket = null;
            }

            Console.WriteLine("Клиент остановлен!");
            Console.ReadKey();
        }

        /// <summary>
        ///  Поток для приема подключений
        /// </summary>
        private void Listen()
        {
            try
            {
                var listenIp = ConfigurationManager.AppSettings["ListenIp"];
                // Прослушиваем по адресу
                IPEndPoint localIP = new IPEndPoint(IPAddress.Parse(listenIp), 0); 
                _listeningSocket.Bind(localIP);

                var maxMessageSize = Int32.Parse(ConfigurationManager.AppSettings["MaxMessageSize"]);

                while (true)
                {
                    Message message;
                    byte[] data = new byte[maxMessageSize];

                    //адрес, с которого пришли данные
                    EndPoint remoteIp = new IPEndPoint(IPAddress.Any, 0);

                    do
                    {
                        _listeningSocket.ReceiveFrom(data, ref remoteIp);
                        message = data.Deserialize<Message>();
                    }
                    while (_listeningSocket.Available > 0);

                    IPEndPoint remoteFullIp = remoteIp as IPEndPoint;

                    Console.WriteLine("{0}:{1} - {2}", remoteFullIp.Address.ToString(), remoteFullIp.Port, message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Stop();
            }
        }

    }
}
