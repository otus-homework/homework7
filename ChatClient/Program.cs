﻿using ChatCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient
{
    class Program
    {
        static void Main(string[] args)
        { 
            Console.Write("Введите ip адрес сервера: ");
            var ipAddress = IPAddress.Parse(Validator.IpAddressValidation(Console.ReadLine()));

            Console.Write("Введите порт сервера: ");
            int remotePort = Validator.IsPort(Console.ReadLine());

            Console.Write("Введите имя пользователя: ");
            var userName = Console.ReadLine();

            var client = new Client(remotePort, ipAddress, userName);
            client.Start();
        } 
    }
}
