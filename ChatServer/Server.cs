﻿using ChatCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer
{
    public class Server
    {
        // порт приема сообщений
        private int _localPort;
        // Сокет
        private Socket _listeningSocket;
        // Список "подключенных" клиентов
        private List<IPEndPoint> _clients = null; 

        public Server(int localPort)
        {
            _localPort = localPort;
            _clients = new List<IPEndPoint>();
        }

        /// <summary>
        /// Старт сервера
        /// </summary>
        public void Start()
        {
            Console.WriteLine("Сервер запущен!");
            try
            {
                // Создание сокета
                _listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                Task listeningTask = new Task(Listen); 
                listeningTask.Start(); 
                listeningTask.Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Stop(); 
            }
        }

        /// <summary>
        /// Остановка сервера
        /// </summary>
        public void Stop()
        {
            if (_listeningSocket != null)
            {
                _listeningSocket.Shutdown(SocketShutdown.Both);
                _listeningSocket.Close();
                _listeningSocket = null;
            }

            Console.WriteLine("Сервер остановлен!");
            Console.ReadKey();
        }

        /// <summary>
        /// Поток для приема подключений
        /// </summary>
        private void Listen()
        {
            try
            {
                var listenIp = ConfigurationManager.AppSettings["ListenIp"];
                //Прослушиваем по адресу
                IPEndPoint localIP = new IPEndPoint(IPAddress.Parse(listenIp), _localPort);
                _listeningSocket.Bind(localIP);

                var maxMessageSize = Int32.Parse(ConfigurationManager.AppSettings["MaxMessageSize"]);
                while (true)
                {
                    StringBuilder builder = new StringBuilder();
                    Message message;
                    byte[] data = new byte[maxMessageSize];

                    //адрес, с которого пришли данные
                    EndPoint remoteIp = new IPEndPoint(IPAddress.Any, 0); 

                    do
                    {
                        _listeningSocket.ReceiveFrom(data, ref remoteIp);
                        message = data.Deserialize<Message>();
                    }
                    while (_listeningSocket.Available > 0);

                    // получаем данные о подключении
                    IPEndPoint remoteFullIp = remoteIp as IPEndPoint; 

                    Console.WriteLine("{0}:{1} - {2}", remoteFullIp.Address.ToString(), remoteFullIp.Port, message); // выводим сообщение

                    bool addClient = true; // Переменная для определения нового пользователя
                    foreach (var client in _clients)
                    {
                        // Если аддресс отправителя данного сообщения совпадает с аддрессом в списке Не добавляем клиента в историю
                        if (String.Equals(client.Address.ToString(), remoteFullIp.Address.ToString(), StringComparison.OrdinalIgnoreCase))
                        {
                            addClient = false;                            
                        }
                    }

                    // Если этого отправителя не было обнаруженно в истории - добавляем клиента в историю
                    if (addClient == true) 
                        _clients.Add(remoteFullIp); // 

                    // Рассылаем сообщения всем клиентам кроме самого отправителя
                    BroadcastMessage(message, remoteFullIp.Address.ToString()); 
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Stop();
            }
        }

        /// <summary>
        /// Метод для рассылки сообщений
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="ip">Ip отправителя</param>
        private void BroadcastMessage(Message message, string ip)
        {
            byte[] data = message.Serialize();

            foreach (var client in _clients)
            {
                // Если аддресс получателя не совпадает с аддрессом отправителя - отправляем сообщение
                if (String.Equals(client.Address.ToString(),ip, StringComparison.OrdinalIgnoreCase))
                {
                    _listeningSocket.SendTo(data, client);
                }
            }
        }

    }
}
