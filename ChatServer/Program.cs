﻿using ChatCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChatServer
{
    class Program
    {
        static void Main(string[] args)
        {            
            Console.Write("Введите порт сервера: ");
            int localPort = Validator.IsPort(Console.ReadLine());

            var server = new Server(localPort);
            server.Start();
        }
    }
}
