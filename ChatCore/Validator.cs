﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChatCore
{
    public class Validator
    {
        public static string IpAddressValidation(string strIp)
        {
            while (!IsIpAddress(strIp))
            {
                Console.WriteLine("IP указан не верно!");
                Console.Write("Введите ip адрес сервера: ");
                strIp = Console.ReadLine();
            }

            return strIp;
        }
        public static bool IsIpAddress(string ipString)
        {
            if (String.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            byte tempForParsing;

            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }

        public static int IsPort(string strPort)
        {
            int remotePort;
            while (!Int32.TryParse(strPort, out remotePort))
            {
                Console.WriteLine("Порт указан не верно!");
                Console.Write("Введите порт сервера: ");
                strPort = Console.ReadLine();
            }

            return remotePort;
        }
    }
}
