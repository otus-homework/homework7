﻿using System;
using System.Net;

namespace ChatCore
{
    [Serializable]
    public class Message
    {
        public string User { get; set; }
        public string Body { get; set; }

        public Message(string user, string body)
        {
            User = user;
            Body = body;
        }

        public override string ToString()
        {
            return $"Пользователь: {User}, Текст: {Body}";
        }
    }
}
